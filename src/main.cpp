//    includes
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "FTDriver.hpp"
#include <geometry_msgs/WrenchStamped.h>

#include <sstream>
#include <iostream>

#include <tf/transform_listener.h>

//    constants
#define MESSAGE_QUEUE 10
#define LOOP_RATE_DEFAULT 60
#define SCALE_FACTOR_DEFAULT 1000000.0
#define SENSOR_IP_ADDRESS_DEFAULT "172.16.0.100"
#define SENSOR_TF_FRAME_DEFAULT "map"
#define BASE_TF_FRAME_DEFAULT "map"

#include "UDP.hpp"



// node process
int main(int argc, char **argv)
{
    // initialize node and message topics
    ros::init(argc, argv, "force_sensor");
    ros::NodeHandle n;
    
    
    std::string sensor_ip_address, sensor_TF,base_TF;
    double sensor_scale_factor, loop_rate;
    bool debugging;
    double handle_mass;

    if (n.getParam("sensor_ip", sensor_ip_address))
    {
        ROS_INFO_STREAM("Got sensor ip: " << sensor_ip_address);
    }
    else
    {
        sensor_ip_address = SENSOR_IP_ADDRESS_DEFAULT;
        ROS_WARN_STREAM("Using default sensor ip: " << sensor_ip_address);
    }


    if (n.getParam("sensor_scale_factor", sensor_scale_factor))
    {
        ROS_INFO_STREAM("Got sensor scale factor: " << sensor_scale_factor);
    } 
    else
    {
        ROS_INFO_STREAM("Using default scale factor: " << SCALE_FACTOR_DEFAULT);
        sensor_scale_factor = SCALE_FACTOR_DEFAULT;
    }       
    
    if (n.getParam("sensor_freq", loop_rate))
    {
        ROS_INFO_STREAM("Got sensor frequency: " << loop_rate << "Hz");
    } 
    else
    {
        ROS_INFO_STREAM("Using default sensor frequency: " << LOOP_RATE_DEFAULT << "Hz");
        loop_rate = LOOP_RATE_DEFAULT;
    }    
    
    if (n.getParam("sensor_TF", sensor_TF))
    {
        ROS_INFO_STREAM("Got sensor transform: " << sensor_TF );
    } 
    else
    {
        ROS_INFO_STREAM("Using default sensor frame: " << SENSOR_TF_FRAME_DEFAULT );
        sensor_TF = SENSOR_TF_FRAME_DEFAULT;
    }    
        
    if (n.getParam("base_TF", base_TF))
    {
        ROS_INFO_STREAM("Got base transform: " << base_TF );
    } 
    else
    {
        ROS_INFO_STREAM("Using default base frame: " << BASE_TF_FRAME_DEFAULT );
        base_TF = BASE_TF_FRAME_DEFAULT;
    }      
    
    handle_mass = 0;
    if (n.getParam("handle_mass", handle_mass))
    {
        ROS_INFO_STREAM("Got handle mass: " << handle_mass << "Kg");
    }


    if (!n.getParam("debug", debugging))
    {
        debugging = false;
    } 
    if(debugging)
    {
        ROS_INFO_STREAM("Debug dump ENABLED");
    }
    else
    {
        ROS_INFO_STREAM("Debug dump: DISABLED");
    }
       
    ros::Publisher wrench_stamped_pub = n.advertise<geometry_msgs::WrenchStamped>( "wrench_stream", MESSAGE_QUEUE );
    ros::Rate loop(loop_rate);

    // initialize messages
    geometry_msgs::WrenchStamped wrench_msg;
    wrench_msg.header.frame_id = base_TF;
    
    // create socket
    FTDriver sensor(sensor_ip_address);
    sensor.start();



    tf::TransformListener listener;
    tf::StampedTransform transform;

    bool found_tf = false;
    long int tf_trys_count = 0;
    while((!found_tf) && (tf_trys_count < 20)){
        tf_trys_count++;
        try{
            listener.lookupTransform(base_TF, sensor_TF,  
                                ros::Time(0), transform);
            found_tf = true;
        }
        catch (tf::TransformException ex){ 
            // do nothing - wait
        }
        ROS_WARN_STREAM("Still waiting transform data - sleep 2s!");
        ros::Duration(2).sleep();
    }   
    if(!found_tf){
        ROS_ERROR("TF data not received - quitting");
        return -1;
    }

    ROS_INFO_STREAM("Callibration starting ");
    FTDriver::Record offset = FTDriver::Record();
    int num_mes = 0;
    
    tf::Vector3 handle_g(0, 0, handle_mass*9.81);
    listener.lookupTransform(sensor_TF, base_TF,   
                                ros::Time(0), transform);
    // transformation to base cs
    tf::Vector3  g = transform.getBasis()*handle_g*sensor_scale_factor;

    while(num_mes < 10){ // dont put more than 100 - overflow int
        // get latest message
        sensor.start();
        
        FTDriver::Record r_tmp = sensor.get();
        if(r_tmp.RDTSequence ){
            offset = offset + r_tmp;
            offset.Fx = offset.Fx + g[0];
            offset.Fy = offset.Fy + g[1];
            offset.Fz = offset.Fz + g[2];
            num_mes++;
        }
    }
    offset = offset/num_mes;
    if(debugging)
            std::cout << offset.Fx << ' ' << offset.Fy << ' ' << offset.Fz << ' ' << offset.Tx << ' ' << offset.Ty << ' ' << offset.Tz << std::endl;


    ROS_INFO_STREAM("Callibration done." );



    // infinit loop
    while (ros::ok())
    {
        
        // get latest message
        sensor.start();
        FTDriver::Record r = sensor.getLatest() - offset;

        // transform the wrench
        tf::Vector3 f,t;
        f = tf::Vector3(r.Fx,r.Fy,r.Fz)/sensor_scale_factor;
        t = tf::Vector3(r.Tx,r.Ty,r.Tz)/sensor_scale_factor;
        
        // transfrom if necessary
        if (base_TF != sensor_TF){
            try{
                listener.lookupTransform(base_TF, sensor_TF,  
                                    ros::Time(0), transform);
                // transformation to base cs
                f = transform.getBasis()*f + handle_g;
                t = transform.getBasis()*t;
            }
            catch (tf::TransformException ex){ 
                ROS_ERROR("%s",ex.what());
            }
        }

        // prepare messages
        // publish WrenchStamped message
        wrench_msg.wrench.force.x = f[0];
        wrench_msg.wrench.force.y = f[1];
        wrench_msg.wrench.force.z = f[2];
        wrench_msg.wrench.torque.x = t[0];
        wrench_msg.wrench.torque.y = t[1];
        wrench_msg.wrench.torque.z = t[2];
        wrench_msg.header.stamp = ros::Time::now();
        wrench_stamped_pub.publish( wrench_msg );

        if(debugging)
            std::cout << wrench_msg.wrench.force.x << ' ' << wrench_msg.wrench.force.y << ' ' << wrench_msg.wrench.force.z << ' ' << wrench_msg.wrench.torque.x << ' ' << wrench_msg.wrench.torque.y << ' ' << wrench_msg.wrench.torque.z << std::endl;

        // end
        ros::spinOnce();
        loop.sleep();
    }
    return 0;
}
