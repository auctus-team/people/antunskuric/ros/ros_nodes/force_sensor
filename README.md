# force_sensor

The original package: https://gitlab.inria.fr/auctus/force_torque_sensor

This is a catkin package which handles the force sensor readings and publishes them in real time to the ros topic.
It publishes the topics `geometry_msgs::WrenchStamped`:
 - `wrench_stream` 


The package has parameters:
- `sensor_ip`
- `sensor_scale_factor` - default 1000000 - outputting force in N
- `sensor_freq` - default 60hz
- `world_frame` - the `tf` frame the wrench should be expressed in - default `world`
- `sensor_frame` - the `tf` frame the sensor is measuring in - default `world`
- `handle_mass` - mass of the handle in kg, always measured by the sensor (it will be removed from the measures)
- `debug` - if `True` the node will dump all the forces and moments to the terminal

Once when package is launched `roslaunch force_sensor visulaise.launch` in the rviz you'be able to see your forces and moments

<img src="imgs/rviz.png">


## Diagram of sensor frames

The scheme of the force sensor with the sensor and worlsd frames are shown here
<img src="imgs/scheme.png" height="500px">

If the sensor and world frame are specified as well as the mass of the handle the node will automatically isolate the `F_ext` and transfrom it to the wolrd frame in real time. 
<img src="imgs/transform.png" >


## Package data workflow 
 The package workflow is shown on this figure 
 
 <img src="imgs/diagram.jpg" width="300px">

